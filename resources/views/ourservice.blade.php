<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from azim.commonsupport.com/Uaques/service.html by HTTrack Website Copier/3.x [XR&CO'2014'], Wed, 28 Jul 2021 12:47:53 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<title>DAMKOFT</title>

<!-- Fav Icon -->
<link rel="icon" href="images/" type="image/x-icon">

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i&amp;display=swap" rel="stylesheet">

<!-- Stylesheets -->
<link href="css/font-awesome-all.css" rel="stylesheet">
<link href="css/flaticon.css" rel="stylesheet">
<link href="css/owl.css" rel="stylesheet">
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/jquery.fancybox.min.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="css/imagebg.css" rel="stylesheet">
<link href="css/jquery-ui.css" rel="stylesheet">
<link href="css/color.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">

</head>


<!-- page wrapper -->
<body class="boxed_wrapper">



    <!-- main header -->

    <header class="main-header">
        <div class="outer-container">
            <div class="header-upper clearfix">
                <div class="upper-left pull-left clearfix  logo-reduce" style="width: 20%;">
                    <figure class="logo-box"><a href="/"><img src="images/damkoftlogo3.png" alt=""></a></figure>

                </div>
                <div class="upper-right pull-right clearfix">
                    <div class="menu-area pull-left">
                        <!--Mobile Navigation Toggler-->
                        <div class="mobile-nav-toggler">
                            <i class="icon-bar"></i>
                            <i class="icon-bar"></i>
                            <i class="icon-bar"></i>
                        </div>
                        <nav class="main-menu navbar-expand-md navbar-light">
                            <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                                <ul class="navigation clearfix">
                                <li><a href="/">Home</a></li>
                                        <li class="current"><a href="/ourservice">Our Services</a></li>
                                        <li><a href="/contact">Contact</a></li>
                                </ul>
                            </div>
                        </nav>
                    </div>

                </div>
            </div>
        </div>

        <!--sticky Header-->
        <div class="sticky-header">
            <div class="auto-container clearfix" >
                <figure style="width: 15%" class="logo-box logo-reduce"><a href="/"><img src="images/damkoftlogo3.png" alt=""></a></figure>
                <div class="menu-area">
                    <nav class="main-menu clearfix">
                        <!--Keep This Empty / Menu will come through Javascript-->
                    </nav>
                </div>
            </div>
        </div>
    </header>


    <!-- main-header end -->

    <!-- Mobile Menu  -->
    <div class="mobile-menu">
        <div class="menu-backdrop"></div>
        <div class="close-btn"><i class="fas fa-times"></i></div>

        <nav class="menu-box">
            <div class="nav-logo"><a href="/"><img src="images/damkoftlogo3.png" alt="" title=""></a></div>
            <div class="menu-outer"><!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header--></div>
            <div class="contact-info">
                <h4>Contact Info</h4>
                <ul>
                    <li>Lagos, Ikorodu, Nigeria</li>
                    <li><a href="tel:07063472570">07063472570</a></li>
                    <li><a href="mailto:info@damkoft.ng">info@damkoft.ng</a></li>
                </ul>
            </div>
            <div class="social-links">
                <ul class="clearfix">
                    <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                    <li><a href="#"><span class="fab fa-facebook-square"></span></a></li>
                    <li><a href="#"><span class="fab fa-instagram"></span></a></li>
                </ul>
            </div>
        </nav>
    </div><!-- End Mobile Menu -->


    <!--Page Title-->
    <section class="page-title centred" style="background-image: url(images/background/damkoftworkspace.jpeg);">
        <div class="auto-container">
            <div class="content-box">
            </div>
        </div>
    </section>
    <!--End Page Title-->


    <!-- fact-counter -->
    <section class="fact-counter padding_bottom_100">
        <div class="auto-container">
            <div class="text-center">
                <h1 class="mb-4"> We Provide All Kinds of Residential & Commercial Water Solutions </h1>
                <p>Bringing clean water to residential, commercial, and industrial places looks different with each application. Water sources, terrain, and population all play a part in determining what technology is required to serve our customers. </p>
            </div>

        </div>
    </section>
    <!-- fact-counter end -->


    <!-- service-section -->

    <!-- service-section end -->


    <!-- testimonial-section  -->

    <!-- testimonial-section end -->


    <!-- request-section -->

    <!-- request-section end -->


    <!-- clients-section  -->
    <section class="clients-section">
        <div class="auto-container">
            <div class="top-title clearfix">
                <div class="text-center">
                    <h1>Other Brand</h1>
                </div>

            </div>
            <div class="clients-carousel owl-carousel owl-theme owl-nav-none owl-dots-none">
                <figure class="image-box"><a href="#"><img src="images/clients/zayfruit2.jpeg" alt=""></a></figure>
                <figure class="image-box"><a href="#"><img src="images/clients/zayfruit.jpeg" alt=""></a></figure>
                <figure class="image-box"><a href="#"><img src="images/clients/zayfruit4.jpeg" alt=""></a></figure>
                <figure class="image-box"><a href="#"><img src="images/clients/zayfruit3.jpeg" alt=""></a></figure>
                <figure class="image-box"><a href="#"><img src="images/clients/zayfruit5.jpeg" alt=""></a></figure>
            </div>
        </div>
    </section>
    <!-- clients-section end -->


    <!-- main-footer -->
    <footer class="main-footer">
        <div class="footer-top">
            <div class="border-shap">
                <div class="border-3" style="background-image: url(images/icons/border-4.png);"></div>
            </div>
            <div class="auto-container">
                <div class="inner-box clearfix">


                </div>
            </div>
        </div>
        <div class="footer-upper">
            <div class="auto-container">
                <div class="widget-section wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                            <div class="logo-widget footer-widget">
                                <div style="" class="footer-logo">
                                    <img src="images/footer-logo.png" style="float: left; width: 100%;" alt="">
                                </div>
                                <div class="text">We earned its place as the worlds largest bottled water brand by delivering pure, safe water all over Nigeria.</div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                            <div class="links-widget footer-widget mt-5">
                                <h3 class="widget-title">About Us</h3>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                            <div class="shediul-widget footer-widget mt-5">
                                <h3 class="widget-title">Business Hours</h3>
                                <div class="widget-content">
                                    <ul class="list clearfix">
                                        <li>Monday-Friday: 9am to 5pm</li>
                                        <li>Saturday: 10am to 4pm</li>
                                        <li>Sunday: Closed</li>
                                    </ul>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                            <div class="contact-widget footer-widget mt-5">
                                <h3 class="widget-title">Contact Us</h3>
                                <div class="widget-content">
                                <ul class="list clearfix">
                                        <li>Ikorodu</li>
                                        <li>Call Us <a href="tel:07063472570">+234 818 557 5385</a></li>
                                        <li>E-mail: <a href="mailto:info@damkoft.ng">info@damkoftwaters.com</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-social pull-right">
            <ul class="social-links clearfix">
                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
            </ul>
        </div>
        <div class="footer-bottom">
            <div class="auto-container">
                <div class="copyright">Copyrights &copy; 2021 Damkoft. All rights reserved.</div>
            </div>
        </div>
    </footer>
    <!-- main-footer end -->



<!--Scroll to top-->
<button class="scroll-top scroll-to-target" data-target="html">
    <span class="fas fa-angle-up"></span>
</button>


<!-- jequery plugins -->
<script src="js/jquery.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.js"></script>
<script src="js/wow.js"></script>
<script src="js/validation.js"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/scrollbar.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/appear.js"></script>

<!-- map script -->
<script src="http://maps.google.com/maps/api/js?key=AIzaSyATY4Rxc8jNvDpsK8ZetC7JyN4PFVYGCGM"></script>
<script src="js/gmaps.js"></script>
<script src="js/map-helper.js"></script>

<!-- main-js -->
<script src="js/script.js"></script>

</body><!-- End of .page_wrapper -->

<!-- Mirrored from azim.commonsupport.com/Uaques/service.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 28 Jul 2021 12:48:08 GMT -->
</html>
