<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from azim.commonsupport.com/ by HTTrack Website Copier/3.x [XR&CO'2014'], Wed, 28 Jul 2021 12:41:04 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<title>DAMKOFT</title>


<link rel="icon" href="images/damkofticon.ico" type="image/x-icon">

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i&amp;display=swap" rel="stylesheet">

<!-- Stylesheets -->
<link href="css/font-awesome-all.css" rel="stylesheet">
<link href="css/flaticon.css" rel="stylesheet">
<link href="css/owl.css" rel="stylesheet">
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/jquery.fancybox.min.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="css/imagebg.css" rel="stylesheet">
<link href="css/color.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">

</head>


<!-- page wrapper -->
<body class="boxed_wrapper">



    <!-- main header -->
    <header class="main-header">
        <div class="outer-container">
            <div class="header-upper clearfix">
                <div class="upper-left pull-left clearfix  logo-reduce" style="width: 20%;">
                    <figure class="logo-box"><a href="/"><img src="images/damkoftlogo3.png" alt=""></a></figure>
                    
                </div>
                
                <div class="upper-right pull-right clearfix">
                    <div class="menu-area pull-left">
                        <!--Mobile Navigation Toggler-->
                        <div class="mobile-nav-toggler">
                            <i class="icon-bar"></i>
                            <i class="icon-bar"></i>
                            <i class="icon-bar"></i>
                        </div>

                        <nav class="main-menu navbar-expand-md navbar-light">
                            <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                                <ul class="navigation clearfix">
                                    <li class="current dropdown"><a href="/">Home</a></li>
                                    <li class="dropdown"><a href="/ourservice">Our Services</a></li>
                                    <li><a href="/contact">Contact</a></li>
                                </ul>
                            </div>
                        </nav>




                    </div>

                </div>
            </div>
        </div>

        <!--sticky Header-->
        <div class="sticky-header">
            <div class="auto-container clearfix" >
                <figure style="width: 15%" class="logo-box logo-reduce"><a href="/"><img src="images/damkoftlogo3.png" alt=""></a></figure>
                <div class="menu-area">
                    <nav class="main-menu clearfix">
                        <!--Keep This Empty / Menu will come through Javascript-->
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- main-header end -->

    <!-- Mobile Menu  -->
    <div class="mobile-menu">
        <div class="menu-backdrop"></div>
        <div class="close-btn"><i class="fas fa-times"></i></div>

        <nav class="menu-box">
            <div class="nav-logo"><a href="/"><img src="images/damkoftlogo3.png" alt="" title=""></a></div>
            <div class="menu-outer"><!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header--></div>
            <div class="contact-info">
                <h4>Contact Info</h4>
                <ul>
                     <li>Lagos, Ikorodu, Nigeria</li>
                    <li><a href="tel:07063472570">07063472570</a></li>
                    <li><a href="mailto:info@damkoft.ng">info@damkoft.ng</a></li>
                </ul>
            </div>
            <div class="social-links">
                <ul class="clearfix">
                    <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                    <li><a href="#"><span class="fab fa-facebook-square"></span></a></li>
                    <li><a href="#"><span class="fab fa-instagram"></span></a></li>
                </ul>
            </div>
        </nav>
    </div><!-- End Mobile Menu -->


    <!-- main-slider -->
    <section class="main-slider">
        <div class="pattern-layer" style="background-image: url(images/icons/pattern-1.png);"></div>
        <div class="main-slider-carousel owl-carousel owl-theme">
            <div class="slide">
                <div class="large-container">
                    <div id="starshine">
                        <div class="shine shine-two shine-1">1</div>
                        <div class="shine shine-two shine-2">2</div>
                        <div class="shine shine-two shine-3">3</div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                            <div class="content-box">
                                <div style="height: 90%;"></div>
                                <h5 style="color: #007bff"> <b>Finest in the bottle</b></h5>
                                <h1>Pure & Fresh out of Nature</h1>
                                <div class="text"></div>

                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                            <div class="image-box">
                                <figure class="image"><img src="images/resource/rebottlewater.png" alt=""></figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slide">
                <div class="large-container">
                    <div id="starshine1">
                        <div class="shine shine-two shine-1">1</div>
                        <div class="shine shine-two shine-2">2</div>
                        <div class="shine shine-two shine-3">3</div>
                    </div>

                    <div class="row clearfix">
                        <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                            <div class="content-box">
                                <div style="height: 90%;"></div>
                                <h5 style="color: #007bff"> <b>Stay healthy</b></h5>
                                <h1>Drink pure live more</h1>


                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                            <div class="image-box">

                                <figure class="image"><img src="images/resource/rebottlewater.png" alt=""></figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slide">
                <div class="large-container">
                    <div id="starshine2">
                        <div class="shine shine-two shine-1">1</div>
                        <div class="shine shine-two shine-2">2</div>
                        <div class="shine shine-two shine-3">3</div>
                    </div>
                    <div class="row clearfix">
                        <div class="col-lg-6 col-md-12 col-sm-12 content-column">
                            <div class="content-box">
                                <div style="height: 90%;"></div>
                                <h5 style="color: #007bff"> <b> Water runs life</b></h5>
                                <h1>Pure water runs life</h1>


                            </div>
                        </div>
                        <div class="col-lg-6 col-md-12 col-sm-12 image-column">
                            <div class="image-box">

                                <figure class="image"><img src="images/resource/rebottlewater.png" alt=""></figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- main-slider end -->


    <!-- feature-section -->
    <section class="feature-section">
        <div class="auto-container">
            <div class="text-center ">
                <h1 class="mb-4">A Trusted Name In<br />Bottled Water Industry</h1>
            </div>
            <div class="inner-content">
                <div class="row clearfix">
                    <div class="col-lg-3 col-md-6 col-sm-12 feature-block">
                        <div class="feature-block-one wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                            <div class="inner-box">
                                <div class="icon-box wow slideInDown" data-wow-delay="250ms" data-wow-duration="1500ms"><i class="flaticon-drop-leaf-table"></i></div>
                                <h3><a href="#">Maximum Purity</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 feature-block">
                        <div class="feature-block-one wow fadeInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
                            <div class="inner-box">
                                <div class="icon-box wow slideInDown" data-wow-delay="250ms" data-wow-duration="1500ms"><i class="flaticon-water"></i></div>
                                <h3><a href="#">5 Steps Filtration</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 feature-block">
                        <div class="feature-block-one wow fadeInLeft" data-wow-delay="600ms" data-wow-duration="1500ms">
                            <div class="inner-box">
                                <div class="icon-box wow slideInDown" data-wow-delay="250ms" data-wow-duration="1500ms"><i class="flaticon-teardrop"></i></div>
                                <h3><a href="#">Cholorine Free</a></h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-12 feature-block">
                        <div class="feature-block-one wow fadeInLeft" data-wow-delay="900ms" data-wow-duration="1500ms">
                            <div class="inner-box">
                                <div class="icon-box wow slideInDown" data-wow-delay="250ms" data-wow-duration="1500ms"><i class="flaticon-water-barrel"></i></div>
                                <h3><a href="#">Quality Certified</a></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- feature-section end -->


    <!-- delivery-section -->

    <!-- delivery-section end -->


    <!-- video-section -->

    <!-- video-section end -->


    <!-- composition-section -->

    <section class="composition-section bg-color-1">
            <div class="border-1" style="background-image: url(images/icons/border-1.png);"></div>
            <div class="border-2" style="background-image: url(images/icons/border-2.png);"></div>
        </div>
        <div class="auto-container">
            <div class="text-center mb-4">
                <h1 class="mb-4">Damkoft Waters<br />Composition</h1>
            </div>
            <div class="upper-content">
                <div class="row clearfix">
                    <div class="col-lg-4 col-md-12 col-sm-12 left-column">
                        <div class="inner-box">
                            <div class="single-item wow slideInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                                <figure class="icon-box">
                                    <span>K+</span>
                                    <img src="images/icons/water-drop-1.png" alt="">
                                </figure>
                                <h3><a href="#">Potassium</a></h3>
                                <h5>2.5 mg/L</h5>
                                <div class="text">Potassium is an essential mineral that is needed by all tissues in the body.</div>
                            </div>
                            <div class="single-item wow slideInLeft" data-wow-delay="300ms" data-wow-duration="1500ms">
                                <figure class="icon-box">
                                    <span>Fl</span>
                                    <img src="images/icons/water-drop-1.png" alt="">
                                </figure>
                                <h3><a href="#">Fluoride</a></h3>
                                <h5>0.5 mg/L</h5>
                                <div class="text">Drinking fluoridated water keeps teeth strong and reduces cavities</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12 image-column">
                        <div class="image-box">
                            <div class="pattern-bg" style="background-image: url(images/icons/pattern-2.png);"></div>
                            <figure class="image wow slideInUp" data-wow-delay="0ms" data-wow-duration="1500ms"><img src="images/resource/water-glass-1.png" alt=""></figure>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12 right-column">
                        <div class="inner-box">
                            <div class="single-item wow slideInRight" data-wow-delay="0ms" data-wow-duration="1500ms">
                                <figure class="icon-box">
                                    <span>Cl</span>
                                    <img src="images/icons/water-drop-1.png" alt="">
                                </figure>
                                <h3><a href="#">Chloride</a></h3>
                                <h5>350a mg/L</h5>
                                <div class="text"> Chloride holds moisture in, it helps control the water that passes through cells.</div>
                            </div>
                            <div class="single-item wow slideInRight" data-wow-delay="300ms" data-wow-duration="1500ms">
                                <figure class="icon-box">
                                    <span>Mg</span>
                                    <img src="images/icons/water-drop-1.png" alt="">
                                </figure>
                                <h3><a href="#">Magnesium</a></h3>
                                <h5>14.5 mg/L</h5>
                                <div class="text"> Magnesium helps lower the risk of cardiovascular disease.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="lower-content clearfix">
                <div class="single-item wow slideInUp" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <h3>Nitrates</h3>
                    <h5>2 mg/L</h5>
                </div>
                <div class="single-item wow slideInUp" data-wow-delay="200ms" data-wow-duration="1500ms">
                    <h3>Bicarbonates</h3>
                    <h5>157 mg/L</h5>
                </div>
                <div class="single-item wow slideInUp" data-wow-delay="400ms" data-wow-duration="1500ms">
                    <h3>Sulphates</h3>
                    <h5>5.6 mg/L</h5>
                </div>
                <div class="single-item wow slideInUp" data-wow-delay="600ms" data-wow-duration="1500ms">
                    <h3>Sodium</h3>
                    <h5>0.4 mg/L</h5>
                </div>
            </div>
        </div>
    </section>

    <!-- composition-section end -->


    <!-- info-section -->

    <!-- info-section end -->




    <!-- news-section -->
        <div style="height: 50px">

        </div>
    <!-- news-section end -->


    <!-- clients-section  -->
    <section class="clients-section">
        <div class="auto-container">
            <div class="top-title clearfix">
                <div class="text-center">
                    <h1>Other Brand(s)</h1>
                </div>

            </div>
            <div class="clients-carousel owl-carousel owl-theme owl-nav-none owl-dots-none">
                <figure class="image-box"><a href="#"><img src="images/clients/zayfruit2.jpeg" alt=""></a></figure>
                <figure class="image-box"><a href="#"><img src="images/clients/zayfruit.jpeg" alt=""></a></figure>
                <figure class="image-box"><a href="#"><img src="images/clients/zayfruit4.jpeg" alt=""></a></figure>
                <figure class="image-box"><a href="#"><img src="images/clients/zayfruit3.jpeg" alt=""></a></figure>
                <figure class="image-box"><a href="#"><img src="images/clients/zayfruit5.jpeg" alt=""></a></figure>
            </div>
        </div>
    </section>
    <!-- clients-section end -->


    <!-- main-footer -->
    <footer class="main-footer">
        <div class="footer-top">
            <div class="border-shap">
                <div class="border-3" style="background-image: url(images/icons/border-4.png);"></div>
            </div>
            <div class="auto-container">
                <div class="inner-box clearfix">


                </div>
            </div>
        </div>
        <div class="footer-upper">
            <div class="auto-container">
                <div class="widget-section wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                            <div class="logo-widget footer-widget">
                                <div style="" class="footer-logo">
                                    <img src="images/footer-logo.png" style="float: left; width: 100%;" alt="">
                                </div>
                                <div class="text">We earned its place as the worlds largest bottled water brand by delivering pure, safe water all over Nigeria.</div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                            <div class="links-widget footer-widget mt-5">
                                <h3 class="widget-title">About Us</h3>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                            <div class="shediul-widget footer-widget mt-5">
                                <h3 class="widget-title">Business Hours</h3>
                                <div class="widget-content">
                                    <ul class="list clearfix">
                                        <li>Monday-Friday: 9am to 5pm</li>
                                        <li>Saturday: 10am to 4pm</li>
                                        <li>Sunday: Closed</li>
                                    </ul>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                            <div class="contact-widget footer-widget mt-5">
                                <h3 class="widget-title">Contact Us</h3>
                                <div class="widget-content">
                                    <ul class="list clearfix">
                                        <li>Ikorodu</li>
                                        <li>Call Us <a href="tel:07063472570">+234 818 557 5385</a></li>
                                        <li>E-mail: <a href="mailto:info@damkoft.ng">info@damkoftwaters.com</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-social pull-right">
            <ul class="social-links clearfix">
                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
            </ul>
        </div>
        <div class="footer-bottom">
            <div class="auto-container">
                <div class="copyright">Copyrights &copy; 2021 Damkoft. All rights reserved.</div>
            </div>
        </div>
    </footer>
    <!-- main-footer end -->



<!--Scroll to top-->
<button class="scroll-top scroll-to-target" data-target="html">
    <span class="fas fa-angle-up"></span>
</button>


<!-- jequery plugins -->
<script src="js/jquery.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.js"></script>
<script src="js/wow.js"></script>
<script src="js/validation.js"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/scrollbar.js"></script>

<!-- main-js -->
<script src="js/script.js"></script>

</body><!-- End of .page_wrapper -->

<!-- Mirrored from azim.commonsupport.com/Uaques/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 28 Jul 2021 12:44:24 GMT -->
</html>

