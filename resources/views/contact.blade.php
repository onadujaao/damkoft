
<!DOCTYPE html>
<html lang="en">
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<title>DAMKOFT</title>
<!-- Fav Icon -->
<link rel="icon" href="images/favicon.ico" type="image/x-icon">
<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i&display=swap" rel="stylesheet">
<!-- Stylesheets -->
<link href="css/font-awesome-all.css" rel="stylesheet">
<link href="css/flaticon.css" rel="stylesheet">
<link href="css/owl.css" rel="stylesheet">
<link href="css/bootstrap.css" rel="stylesheet">
<link href="css/jquery.fancybox.min.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="css/imagebg.css" rel="stylesheet">
<link href="css/jquery-ui.css" rel="stylesheet">
<link href="css/jquery.bootstrap-touchspin.css" rel="stylesheet">
<link href="css/color.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/responsive.css" rel="stylesheet">
</head>
<!-- page wrapper -->
<body class="boxed_wrapper">
 <!-- main header -->
 <header class="main-header">
        <div class="outer-container">
            <div class="header-upper clearfix">
                <div class="upper-left pull-left clearfix  logo-reduce" style="width: 10%;">
                    <figure class="logo-box"><a href="/"><img src="images/damkoftlogo3.png" alt=""></a></figure>
                </div>
                <div class="upper-right pull-right clearfix">
                    <div class="menu-area pull-left">
                        <!--Mobile Navigation Toggler-->
                        <div class="mobile-nav-toggler">
                            <i class="icon-bar"></i>
                            <i class="icon-bar"></i>
                            <i class="icon-bar"></i>
                        </div>
                        <nav class="main-menu navbar-expand-md navbar-light">
                            <div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
                                <ul class="navigation clearfix">
                                <li><a href="/">Home</a></li>
                                        <li><a href="/ourservice">Our Services</a></li>
                                        <li class="current"><a href="/contact">Contact</a></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!--sticky Header-->
        <div class="sticky-header">
            <div class="auto-container clearfix" >
                <figure style="width: 10%" class="logo-box logo-reduce"><a href="/"><img src="images/damkoftlogo3.png" alt=""></a></figure>
                <div class="menu-area">
                    <nav class="main-menu clearfix">
                        <!--Keep This Empty / Menu will come through Javascript-->
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- main-header end -->
 
    <!-- Mobile Menu  -->
    <div class="mobile-menu">
        <div class="menu-backdrop"></div>
        <div class="close-btn"><i class="fas fa-times"></i></div>
        <nav class="menu-box">
            <div class="nav-logo"><a href="/"><img src="images/damkoftlogo3.png" alt="" title=""></a></div>
            <div class="menu-outer"><!--Here Menu Will Come Automatically Via Javascript / Same Menu as in Header--></div>
            <div class="contact-info">
                <h4>Contact Info</h4>
                <ul>
                    <li>Lagos, Ikorodu, Nigeria</li>
                    <li><a href="tel:07063472570">+234 818 557 5385</a></li>
                    <li><a href="mailto:info@damkoft.ng">info@damkoftwaters.com</a></li>
                </ul>
            </div>
            <div class="social-links">
                <ul class="clearfix">
                    <li><a href="#"><span class="fab fa-twitter"></span></a></li>
                    <li><a href="#"><span class="fab fa-facebook-square"></span></a></li>
                    <li><a href="#"><span class="fab fa-instagram"></span></a></li>
                </ul>
            </div>
        </nav>
    </div><!-- End Mobile Menu -->
    <!--Page Title-->
    <section class="page-title centred" style="background-image: url(images/background/page-title-new.jpeg); background-size: cover;">
        <div class="auto-container" ;>
            <div class="content-box">
                {{--  <h1>Contact us</h1>  --}}
            </div>
        </div>
    </section>
    <!--End Page Title-->
    <!-- contact-section -->
    <section class="contact-section sec-pad">
        <div class="auto-container">
            @if(Session::has('message'))
        <p class="alert alert-success">{{Session::get('message') }}</p>
        @endif
            <div class="row clearfix">
                <div class="col-lg-4 col-md-12 col-sm-12 info-column">
                    <div class="info-box">
                        <h2 style="line-height: 170%">Contact Information</h2>
                        <ul class="info-list clearfix wow fadeInUp" data-wow-delay="00ms" data-wow-duration="1500ms">
                            <li><i class="fas fa-phone"></i><strong>Call Us</strong> <a href="tel:12078761059">+234 818 557 5385</a></li>
                            <li><i class="fas fa-map-marker-alt"></i>Ikorodu, Nigeria</li>
                            <li><i class="fas fa-envelope"></i><strong>E-mail</strong> <a href="mailto:info@damkoft.ng">info@damkoftwatwers.com</a></li>
                            <li><i class="fas fa-clock"></i>Monday-Friday: 9am to 5pm<br />Saturday: 10am to 4pm</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-8 col-md-12 col-sm-12 inner-column">
                    <div class="inner-box">
                        <h2 style="line-height: 170%">Send a Message</h2>
                        <div class="form-inner">
                            <form method="post" action="{{url('sendmail')}}">
                            @csrf
                                <div class="row clearfix">
                                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                    <label>Your Name</label>
                                        <input type="text" name="username" required>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                        <label>Email address</label>
                                        <input type="email" name="email" required>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                        <label>Subject</label>
                                        <input type="text" name="subject" required>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 form-group">
                                        <label>Phone</label>
                                        <input type="text" name="phone" required>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 form-group">
                                        <label>Message</label>
                                        <textarea name="message"></textarea>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 form-group message-btn">
                                        <button type="submit" name="submit-form">submit now</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- contact-section end -->
    {{--  <!-- map-section -->
    <section class="map-section">
        <div class="google-map-area">
            <div
                class="google-map"
                id="contact-google-map"
                data-map-lat="40.712776"
                data-map-lng="-74.005974"
                data-icon-path="images/icons/map-marker.png"
                data-map-title="Brooklyn, New York, United Kingdom"
                data-map-zoom="12"
                data-markers='{
                    "marker-1": [40.712776, -74.005974, "<h4>Branch Office</h4><p>77/99 New York</p>","images/icons/map-marker.png"]
                }'>
            </div>
        </div>
    </section>
    <!-- map-section end -->  --}}
    <!-- main-footer -->
    <footer class="main-footer">
        <div class="footer-top">
            <div class="border-shap">
                <div class="border-3" style="background-image: url(images/icons/border-4.png);"></div>
            </div>
            <div class="auto-container">
                <div class="inner-box clearfix">
                </div>
            </div>
        </div>
        <div class="footer-upper">
            <div class="auto-container">
                <div class="widget-section wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                    <div class="row clearfix">
                        <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                            <div class="logo-widget footer-widget">
                                <div style="width: 20%" class="footer-logo">
                                    <img src="images/dlogo.png" style="float: left; padding-right: 20px;" alt="">
                                </div>
                                <div class="text">We earned its place as the world's largest bottled water brand by delivering pure, safe water all over Nigeria.</div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                            <div class="links-widget footer-widget">
                                <h3 class="widget-title"><a href="" >About Us</a></h3>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                            <div class="shediul-widget footer-widget">
                                <h3 class="widget-title"><a href="" >Services</a></h3>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 footer-column">
                            <div class="contact-widget footer-widget">
                                <h3 class="widget-title"><a href="" >Contact Us</a></h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-social pull-right">
            <ul class="social-links clearfix">
                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
            </ul>
        </div>
        <div class="footer-bottom">
            <div class="auto-container">
                <div class="copyright">Copyrights &copy; 2021 Damkoft. All rights reserved.</div>
            </div>
        </div>
    </footer>
    <!-- main-footer end -->
<!--Scroll to top-->
<button class="scroll-top scroll-to-target" data-target="html">
    <span class="fas fa-angle-up"></span>
</button>
<!-- jequery plugins -->
<script src="js/jquery.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.js"></script>
<script src="js/wow.js"></script>
<script src="js/validation.js"></script>
<script src="js/jquery.fancybox.js"></script>
<script src="js/scrollbar.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/appear.js"></script>
<script src="js/jquery.bootstrap-touchspin.js"></script>
<!-- map script -->
<script src="http://maps.google.com/maps/api/js?key=AIzaSyATY4Rxc8jNvDpsK8ZetC7JyN4PFVYGCGM"></script>
<script src="js/gmaps.js"></script>
<script src="js/map-helper.js"></script>
<!-- main-js -->
<script src="js/script.js"></script>
</body><!-- End of .page_wrapper -->
<!-- Mirrored from azim.commonsupport.com/Uaques/contact.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 28 Jul 2021 12:49:31 GMT -->
</html>
