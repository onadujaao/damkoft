<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
class ContactController extends Controller
{
     /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('contact');
    }
    public function sendMail(Request $request) {
        $data = array(
                        'username'=>$request->username,
                        'phone'=>$request->phone,
                        'email'=>$request->email,
                        'subject'=>$request->subject,
                        'message'=>$request->message,
                    );

        Mail::send('mail', $data, function($message) {
           $message->to('onadujaao@gmail.com', 'Damkoft')->subject
              ('Contact Us from customer');
           $message->from('onadujaao@gmail.com','Onaduja Adedayo');
        });

        Session::flash('message','Your message was sent successfully!');
       return back();
     }
}
